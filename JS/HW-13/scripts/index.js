// Описати своїми словами різницю між функціями setTimeout() і setInterval()`
// Функції setTimeout() та setInterval() є методами JavaScript, які дозволяють виконувати певний код через певний проміжок часу.
// Основна різниця між цими методами полягає в тому, що setTimeout() виконує код лише один раз через певний проміжок часу, тоді як setInterval() 
// виконує код багато разів через рівні проміжки часу.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Якщо в функцію setTimeout() передати нульову затримку, код, який потрібно виконати, не буде виконаний миттєво. 
// Натомість, він буде доданий в чергу завдань на виконання, і виконання коду розпочнеться, коли браузер звільнить 
// головний потік виконання JavaScript.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Функція setInterval() встановлює повторювану задачу, яка буде виконуватися через певний проміжок часу. Якщо не зупиняти цю задачу, 
// вона буде продовжувати виконуватися, незважаючи на те, чи є вона вам потрібною чи ні. Це може призвести до зайвого використання ресурсів
// браузера та зниження продуктивності.


const img = document.querySelectorAll('.image-to-show');
const btnStop = document.querySelector('.js-stop');
const btnStart = document.querySelector('.js-start');

let slide = 1;

const hiddenImg = () => {
    for(let i =0 ; i < img.length; i++) {
        img[i].classList.add('hidden')
    }
};

const showImg = () => {
    img[slide].classList.remove('hidden');
    slide++;
};


let intervalId = null;

intervalId = setInterval(() => {
    hiddenImg();
    showImg();
    if(slide > img.length-1){
        slide = 0;
    }
}, 3000);



btnStart.addEventListener('click', 
handleClick=()=>{
    clearInterval(intervalId);

    setTimeout(()=>{
        intervalId = setInterval(() => {
            hiddenImg();
            showImg();
            if(slide > img.length-1){
                slide = 0;
            }
        }, 3000);
    }, 0);
});
  


btnStop.addEventListener('click', () => {
    clearInterval(intervalId);
})



