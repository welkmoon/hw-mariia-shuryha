// Опишіть своїми словами, що таке метод об'єкту
// Метод об'єкту - це функція, яка визначена в межах об'єкту і використовується для взаємодії з цим об'єктом.

// Який тип даних може мати значення властивості об'єкта?
// Будь-який

// Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає, що коли змінна отримує значення об'єкта, то вона фактично отримує посилання на об'єкт, а не сам об'єкт. 


// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

const createNewUser = () => {
    const firstName = prompt('Enter your name');
    const lastName = prompt('Enter your lastname');

    const newUser ={
        firstName,
        lastName,
        getLogin() {
            return this.firstName.slice(0,1).toLowerCase() + this.lastName.toLowerCase();
        }
    };

    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());