"use strict"

const form = document.querySelector('.password-form');
const icon = document.querySelectorAll('.fas')
const error = document.createElement('span');

icon.forEach(el => {

    el.addEventListener('click', (event) => {
       
        const input = document.querySelector(`.${el.dataset.icon}`);

        if (input.type === 'password') {
            input.setAttribute('type', 'text');
            el.setAttribute('class', 'fas fa-eye-slash icon-password');
    
        } else {
            input.setAttribute('type', 'password');
            el.setAttribute('class', 'fas fa-eye icon-password');
        }


    })
})


    
form.addEventListener('submit', (e) => {
    e.preventDefault();
  
    const inputs = e.target.querySelectorAll('input');

    const values = {};

    inputs.forEach((elem, index) => {
        values[index] = elem.value;
    })
    if (values[0] === values[1]) {
        alert('You are welcome')
    } else {
        error.style.color = 'red';
        error.innerText = "Потрібно ввести однакові значення!";
        const inputError = document.querySelector('.password-repead');
        inputError.after(error);
    }
    e.target.reset();
 
})


