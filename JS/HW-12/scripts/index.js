// Чому для роботи з input не рекомендується використовувати клавіатуру?

// Зазвичай для роботи зі введенням даних в програму можна використовувати різні пристрої введення, включаючи клавіатуру. 
// Однак, для деяких видів задач, особливо тих, що пов'язані з обробкою даних та розпізнаванням тексту, клавіатура не є найкращим інструментом.
// Основна причина полягає в тому, що клавіатура не надає контексту, який можна було б використовувати для розуміння тексту. 
// Наприклад, клавіша "5" може означати різні речі в залежності від контексту: якщо це введення дати, то вона може означати "травень" (May),
//  а якщо це введення часу, то вона може означати "п'ята година" (5 o'clock). Також клавіатура не може передати інформацію про 
//  інтонацію та акцент, які можуть впливати на розуміння тексту.

"use strict"

const buttons = document. querySelectorAll('.btn');
// console.log(buttons);
window.addEventListener('keydown', (event) => {
    // console.log(event);
    buttons.forEach((el) => {
        el.style.backgroundColor = 'black';
       if ((el.innerText === event.key.toUpperCase()) || (el.innerText === event.key)) {
        el.style.backgroundColor = 'blue';
       }
    }) 
})
