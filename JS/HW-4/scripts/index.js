// -----Описати своїми словами навіщо потрібні функції у програмуванні.-----
// Функції потрібні щоб не повторювати той самий код у багатьох місцях

// -----Описати своїми словами, навіщо у функцію передавати аргумент.-----
// Передача аргументів у функцію дозволяє нам зробити функції більш 
// гнучкими та повторно використовуваними, 
// оскільки ми можемо передавати різні значення в різні виклики функції

// -----Що таке оператор return та як він працює всередині функції?-----
// Оператор return використовується для повернення значення з функції. 
// Це означає, що коли функція викликається і доходить до оператора return, 
// вона завершує своє виконання та повертає значення виклику функції.
// Якщо оператор return не вказаний у функції, то функція поверне значення undefined.



// Отримання значень від користувача
    const validateNumber = number => !(number === null || number === "" || isNaN(number)); // !number


    const getNumber = (message = "Enter your number") => {
    
        let userNumber;
    
        do {
            userNumber = prompt(message);
        } while (!validateNumber(userNumber))
    
        return +userNumber;
    }

    
    const calcAdd = (a,b) => a+b;
    const calcSubtract = (a, b) => a-b;
    const calcmMultiply = (a, b) => a*b;
    const calcDivide = (a, b) => a/b;
    
    
    const calcNumbers = (operation) => {
        switch (operation) {
            case '+': { 
              const a = getNumber("Введіть перше число");
              const b = getNumber("Введіть друге число");
                return calcAdd(a,b);
            };
    
            case '-': { 
              const a = getNumber("Введіть перше число");
              const b = getNumber("Введіть друге число");
                return calcSubtract(a, b);
            };
    
            case '*': { 
              const a = getNumber("Введіть перше число");
              const b = getNumber("Введіть друге число");
                return calcmMultiply(a, b);
            };
    
            case '/': { 
              const a = getNumber("Введіть перше число");
              const b = getNumber("Введіть друге число");
                return calcDivide(a, b);
            };
            
        }
    }

    let getOperation;
      do {
          getOperation = prompt("Enter operation (+, -, *, /):");
      } while (getOperation !=="+" && getOperation !== "-" && getOperation !== "*" && getOperation !== "/");
    
    let figureType;
    
    
    console.log(calcNumbers(getOperation));