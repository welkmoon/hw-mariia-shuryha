'use strict';

const themeBlue = document.createElement('link');
themeBlue.rel="stylesheet"
themeBlue.href= "./styles/theme-blue.css"

const btnChangeTheme = document.querySelector('.btn-change-theme');

btnChangeTheme.addEventListener('click', (e) => {
	const theme = e.target.dataset.theme;
	changeTheme(theme);
	localStorage.setItem('theme', theme);
});

const changeTheme = (theme) => {
	if (theme === 'blue') { 
		document.head.append(themeBlue); 
		btnChangeTheme.dataset.theme = 'green'; 
	} else {
		btnChangeTheme.dataset.theme = 'blue';
		themeBlue.remove();
	}
}

const currantTheme = localStorage.getItem('theme');

if (!currantTheme) {
	changeTheme('green');
} else {
	changeTheme(currantTheme);
}

